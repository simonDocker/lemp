# NGINX PHP-FPM MYSQL

## Sources:
https://gitlab.com/Reedon/symfony  
https://github.com/sebastianbrosch/docker-stack-web-server  
https://github.com/maciejslawik/docker-php-fpm-xdebug/blob/7.2/Dockerfile  

# TODO
- mysql logs
- make fix.sh unnecessary
- adjust start.sh
- add .env variables for ports

# Bug // alias I dont know why this happens

`docker-compose.yml` always tries to map an directory to an file.  
```
 ./logs/php/fpm-php.www.log:/var/log/fpm-php.www.log
```

The Dockerfile `bin/php/Dockerfile` creates the file

```
# Create php log file
RUN touch /var/log/fpm-php.www.log && chmod 777 /var/log/fpm-php.www.log
```

why does docker-compose assume that `fpm-php.www.log` is an directory?

## Quickfix

`fix.sh` creates the file first so docker-compose cannot assume that this is an directory