#!/bin/bash
sudo rm -Rf ./bin/mysql/data/*

LOG="./logs/php/fpm-php.www.log"
if [ -d "$LOG" ]; then
    echo "kill"
    rm -Rf $LOG;
fi

if [ ! -f "$LOG" ]; then
    touch ./logs/php/fpm-php.www.log
fi